package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods {
	
	public DuplicateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@name='submitButton']")
	private WebElement createButton;
	public ViewLead clickCreateButton() {
		click(createButton);
		return  new ViewLead();
	}
}
