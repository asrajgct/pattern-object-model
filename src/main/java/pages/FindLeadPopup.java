package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPopup extends ProjectMethods{

	public FindLeadPopup() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//input[@name='firstName'])")
	private WebElement firstname;
	public FindLeadPopup typeFirstName(String data) {
		type(firstname, data);
		return this;
	}

	@FindBy(xpath = "//button[text()='Find Leads']")
	private WebElement findbutton;
	public FindLeadPopup clickFindButton() {
		click(findbutton);
		return this;
	}
	@FindBy(xpath="((//a[@class='linktext'])[1])")
	private WebElement userid;
	public FindLeadPopup getUserId() {
		Id = getText(userid);
		System.out.println(Id);
		return this;
	}
	@FindBy(xpath="((//a[@class='linktext'])[1])")
	private WebElement userid1;
	public MergeLead clickUserId() {
		click(userid1);
		switchToWindow(0);
		return new MergeLead();
	}
}
