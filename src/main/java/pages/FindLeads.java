package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	//Delete Lead
	@FindBy(xpath = "//span[text()='Phone']")
	private WebElement phonetab;
	public FindLeads clickPhoneTab() {
		click(phonetab);
		return this;
	}
	
	@FindBy(xpath = "//input[@name='phoneNumber']")
	private WebElement phoneno;
	public FindLeads typePhone(String data) {
		type(phoneno, data);
		return this;
	}
	
	@FindBy(xpath = "//button[text()='Find Leads']")
	private WebElement findleadbutton;
	public FindLeads clickFindleadButton() {
		click(findleadbutton);
		return this;
	}
	@FindBy(xpath = "(//a[@class='linktext'])[4]")
	private WebElement getUser;
	public FindLeads getUserId() {
		Id = getText(getUser);
		System.out.println(Id);
		return this;
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[6]")
	private WebElement fnametext;
	public FindLeads getFirstName() {
		firstnametext = getText(fnametext);
		return this;
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[7]")
	private WebElement lnametext;
	public FindLeads getlastName() {
		lastnametext = getText(lnametext);
		return this;
	}
	@FindBy(xpath = "(//a[@class='linktext'])[4]")
	private WebElement clickUser;
	public ViewLead clickUserId() {
		click(clickUser);
		return new ViewLead();
	}
	
	@FindBy(xpath = "//input[@name='id']")
	private WebElement userid;
	public FindLeads typeUserId() {
		type(userid, Id);
		return this;
	}
	
	@FindBy(xpath = "//div[@class='x-paging-info']")
	private WebElement messloc;
	public FindLeads getErrorMessage() {
		String errormessage = getText(messloc);
		System.out.println(errormessage);
		return this;
	}
	//Edit Lead
	@FindBy(xpath = "((//input[@name='firstName'])[3])")
	private WebElement firstname;
	public FindLeads typeFirstName(String data) {
		type(firstname, data);
		return this;
	}
	
	@FindBy(xpath = "(//a[@class='linktext'])[6]")
	private WebElement clickFName;
	public ViewLead clickFirstName() {
		click(clickFName);
		return new ViewLead();
	}
	
	//Duplicate Lead
	@FindBy(xpath =  "//span[text()='Email']")
	private WebElement emailTab;
	public FindLeads clickEmailTab() {
		click(emailTab);
		return this;
	}
	@FindBy(xpath = "//input[@name='emailAddress']")
	private WebElement emailtext;
	public FindLeads typeEmail(String data) {
		type(emailtext, data);
		return this;
	}
	
}
