package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods {

	public MergeLead(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "((//img[@alt='Lookup'])[1])")
	private WebElement firstIcon;
	public FindLeadPopup clickFirstIcon() {
		click(firstIcon);
		switchToWindow(1);
		return new FindLeadPopup();		
	}
	
	@FindBy(xpath = "((//img[@alt='Lookup'])[2])")
	private WebElement secondIcon;
	public FindLeadPopup clickSecondIcon() {
		click(secondIcon);
		switchToWindow(1);
		return new FindLeadPopup();	
	}
	
	@FindBy(xpath = "//a[text()='Merge']")
	private WebElement mergeButton;
	public ViewLead clickMergeButton() {
		click(mergeButton);
		System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().accept();
		return new ViewLead();
		//return new FindLeadPopup();	
	}
}
