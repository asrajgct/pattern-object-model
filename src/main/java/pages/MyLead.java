package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLead extends ProjectMethods {
	
	public MyLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement CreateLead;
	public CreateLead clickCreateLeads() {
		click(CreateLead);
		return new CreateLead();
	}
	@FindBy(xpath = "//a[text()='Find Leads']")
	private WebElement findleadlink;
	public FindLeads clickFindLeadslink() {
		click(findleadlink);
		return new FindLeads();
	}
	
	@FindBy(linkText = "Merge Leads")
	private WebElement mergeLeadLink;
	public MergeLead clickMergeLeadlink() {
		click(mergeLeadLink);
		return new MergeLead();
	}

}
