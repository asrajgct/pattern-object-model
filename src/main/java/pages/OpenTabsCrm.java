package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class OpenTabsCrm extends ProjectMethods {

	public OpenTabsCrm() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="updateLeadForm_companyName")
	private WebElement textCompany;
	public OpenTabsCrm clearText() {
		click(textCompany);
		return this;
	}

	@FindBy(id="updateLeadForm_companyName")
	private WebElement typeCompany;
	public OpenTabsCrm typeCompany(String data) {
		type(typeCompany, data);
		return this;
	}

	@FindBy(xpath = "//input[@name='submitButton']")
	private WebElement updateButton;
	public ViewLead clickUpdateButton() {
		click(updateButton);
		return new ViewLead();

	}
}
