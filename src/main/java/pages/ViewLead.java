package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	public String DupFirstName;
	public String DupLastName;
	public String DupUid;
//Delete Lead
	@FindBy(xpath="//a[text()='Delete']")
	private WebElement clickDelete;
	public MyLead ClickDeleteButton() {
		click(clickDelete);
		return new MyLead();
	}
//merge lead
	@FindBy(xpath="//a[text()='Find Leads']")
	private WebElement clickFindLeadlink;
	public FindLeads clickFindLead() {
		click(clickFindLeadlink);
		return new FindLeads();
	}

//edit Lead
	@FindBy(linkText="Edit")
	private WebElement editLink;
	public OpenTabsCrm clickEdit() {
		click(editLink);
		return new OpenTabsCrm();
	}
	
	@FindBy(id="viewLead_companyName_sp")
	private WebElement curcompany;
	public ViewLead getCompany() {
		String company = getText(curcompany);
		if (company.contains("TCS")) {

			System.out.println("Company name has beend updated");
		} else {
			System.out.println("Company name has not beend updated");

		}
		return this;
	}
	// Duplicate Lead

	
	@FindBy(linkText="Duplicate Lead")
	private WebElement duplicateLink;
	public DuplicateLead clickDuplicate() {
		click(duplicateLink);
		return new DuplicateLead();
	}
	
	@FindBy(xpath = "//span[@id='viewLead_firstName_sp']")
	private WebElement duplicatefname;
	public ViewLead getDuplicatefname() {
		DupFirstName = getText(duplicatefname);
		return this;
	}
	
	@FindBy(xpath = "//span[@id='viewLead_lastName_sp']")
	private WebElement duplicatelname;
	public ViewLead getDuplicatelname() {
		DupLastName = getText(duplicatelname);
		return this;
	}
	
	@FindBy(xpath = "//span[@id='viewLead_companyName_sp']")
	private WebElement duplicateuserid;
	public ViewLead getDuplicateUserid() {
		DupUid = getText(duplicateuserid);
		if ((firstnametext.equalsIgnoreCase(DupFirstName) && (lastnametext.equalsIgnoreCase(DupLastName) && (Id != DupUid)))) {

			System.out.println("Duplicate lead has been created and the id is " +DupUid );

		} else 
		{
			System.out.println("Duplicate ID has not been created");
		}

		return this;
	}
}

