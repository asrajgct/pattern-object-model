package testcases;




import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC002_EditLead";
		testDescription="Editing a company";
		authors="Suresh";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="Edit";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String firstname, String comp) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLinkCRM()
		.clickLeads()
		.clickFindLeadslink()
		.typeFirstName(firstname)
		.clickFindleadButton()
		.clickFirstName()
		.clickEdit()
		.clearText()
		.typeCompany(comp)
		.clickUpdateButton()
		.getCompany();



	}
}