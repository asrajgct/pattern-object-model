package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_DuplicateLead extends ProjectMethods{


	@BeforeTest
	public void setData() {
		testCaseName="TC003_DuplicateLead";
		testDescription="Deleting a new Lead";
		authors="Suresh";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="Duplicate";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String email) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLinkCRM()
		.clickLeads()
		.clickFindLeadslink()
		.clickEmailTab()
		.typeEmail(email)
		.clickFindleadButton()
		.getUserId()
		.getFirstName()
		.getlastName()		
		.clickUserId()
		.clickDuplicate()
		.clickCreateButton()
		.getDuplicatefname()
		.getDuplicatelname()
		.getDuplicateUserid();

	}
}