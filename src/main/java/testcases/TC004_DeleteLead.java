package testcases;




import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC004_DeleteLead";
		testDescription="Deleting a new Lead";
		authors="Suresh";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="Delete";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String phoneno) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLinkCRM()
		.clickLeads()
		.clickFindLeadslink()
		.clickPhoneTab()
		.typePhone(phoneno)
		.clickFindleadButton()
		.getUserId()
		.clickUserId()
		.ClickDeleteButton()
		.clickFindLeadslink()
		.typeUserId()
		.clickFindleadButton()
		.getErrorMessage();

	}
}