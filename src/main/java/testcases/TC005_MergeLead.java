package testcases;




import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC005_MergeLead";
		testDescription="merging two Leads";
		authors="Suresh";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="Merge";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String fname1, String fname2) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLinkCRM()
		.clickLeads()
		.clickMergeLeadlink()
		.clickFirstIcon()
		.typeFirstName(fname1)
		.clickFindButton()
		.getUserId()
		.clickUserId()
		.clickSecondIcon()
		.typeFirstName(fname2)
		.clickFindButton()
		.clickUserId()
		.clickMergeButton()
		.clickFindLead()
		.typeUserId()
		.clickFindleadButton()
		.getErrorMessage();

	}
}